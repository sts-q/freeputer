;==============================================================================
; :section:   stdmodules.rabbit
;             
;------------------------------------------------------------------------------


"stdmodules"loading


;==============================================================================
; :section:   dotdoc.rabbit
;             dot documentation
[
;------------------------------------------------------------------------------
ddw {w -- // dot documentation of word w}
	func
	| nothing?* 	| zap "         is not defined." out lf 
	others		| lf   unlist out^^ lf show^ print^  lf out lf

;--------------------------------------------------

	:-out {} show 18 String.justify-left print
	:_print {} unswons first out  ( first Symbol.undotted second "."print out )each lf
	:-print {}   ( unlist -out^   show print  lf )each
	:p {Module (name docstring) -- Module b}
		first Symbol.undotted
		| dup length 2 = not	| zap false
		|     first over =	|     true
		others			|     false
		 
dda {w --  // dot documentation of module api}
		allfuncs(2 take)map   (p)filter   ( first )sorton 
		(first Symbol.undotted second Symbol.underscore? )split
		over nil?( nip )( _print^ )if
		-print
		zap

ddm {w -- // dot documentation of module}
		"-----------------------------------------------------------------------------"
		lf out lf
		dup (__man)cons Symbol.dotted
		| dup defined?	| call   zap
		others		| zap    (__info)cons Symbol.dotted call out lf
		

	:u-out {m --}
		dup (show 15 String.justify-left print)dip  (__info)cons Symbol.dotted
		dup defined?(call String.lines second out)(zap)if
	:-print-unit {m --}  dup (__man)cons Symbol.dotted defined? (ddm)(zap)if
	:-print-members {} sort ( u-out lf )each
	:-print {} unlist -print-unit^ -print-members
ddu {w -- // dot documentation of Unit of Module w}
		Unit.units^
		| over nil? 			| 2zap "         no Unit for Module found." out lf
		| unswons^ ( second^ member?)i*	| zap nip -print
		|          ( first^ =	    )i* | zap nip -print
		others				| nip recur


	:unit-names {} Unit.units flatten flatten
	:-print {} show 15 String.justify-left print
ddus {--  // dot documentation of Units: list units}
		"-----------------------------------------------------------------------------"
		out lf
		Unit.units ( unlist -print^   sort( out )each lf )each
		lf
		Module.names unit-names
		"not listed:" -print 2dup LSet.difference ( out )each lf
		"not defined:"-print swap LSet.difference ( out )each lf
		

;------------------------------------------------------------------------------

help {-- // print help}
"-----------------------------------------------------------------------------
help
Some in repl useful words.

ddw             dot (print) documentation of word       sample:   `ddw ddw
dda             dot documentation of module-api                `String dda
ddm             dot documentation of module                    `String ddm
ddu             dot documentation of unit of module            `String ddu
ddus            dot documentation of all units                         ddus
Module.names    print list of all modules
atoms           print a list of all atoms and definitions
xx              print stack and break to repl
lf		print a newline
out		print a stack value 
VM.stats        print some stats
Stacks.print    print stacks ds and ss
terms		print some explanations about frequently used terms
types		print help about types used in stack comments

Every 'rabbit>' prompt starts with an empty stack."
lf out lf


terms {--  // print help about frequently used terms}
"-----------------------------------------------------------------------------
terms
Frequently used terms.

fun             function from stack to stack: atom or definition
atom		function written in freelang, built into Rabbit-vm
definition      function defined in Joy
func            (name * docstring * fn)-tuple: representation of a fun
fn              atom or quotation
quotation	joy program

word		symbol
name		word or symbol naming a function
docstring       string of its own type, mainly used for documentation of funs


In Joy there is

	list == concatenation == quotation == program == data == stack == list

Program construction happens by concatenation of items to lists.
This list is called concatenation or quotation, well its still a list.
And these lists are the program that gets executed at some time or not.
The stack is a list, too. But with its own set of operators working on it."
lf out lf

types {--  // types}
"-----------------------------------------------------------------------------
types
Types used in stack comments.

i j k l m n	integer
b		bool
s		string
u v w		symbol
w		word
l 		list
q p		quotation, program
il li		integer list, list of integers
ws us		words, symbols, that is list of words, symbols, ...
a b c x y z	anything

ts		tokens: what the parser returns
"
lf out lf


;==============================================================================
; :section:   CSet
;------------------------------------------------------------------------------
module CSet
interface _{}
__info {}
"CSet
counting set
	type counter	(name  int)
	type cset	counter list
"
__man {} __info Root.out 

;------------------------------------------------------------------------------
new {-- cset}  nil
out {cset  --}   ( Root.out lf )each
sort {cset  --  cset  // sorted on counts}  ( second )sorton
keys {cset -- l  // list of keys of CSet}   ( first )map
member? {cset name  --  b}   assoc nothing = not

	:inc-count {l  --  l} unlist inc pair   
	:found?	{name cset pair cset  --  name cset pair cset b} (zap first nip = )i*
insert {cset name -- cset}
		swap  nil^
		| nil?*		| zap  ( 1 pair )dip cons
		| uncons found? |      ( inc-count swons )dip ++ nip
		others		|      swons^ recur

__test {} new  (a b c d e a b c d a b c a b a)(insert)each sort lf out

end-of-module CSet


;==============================================================================
; :section:   list Set
;------------------------------------------------------------------------------
LSet.__info {}"LSet
operations on lists as sets"

LSet.insert 		{l x -- l}   2dup  member?( zap )( swons )if

LSet.difference 	{l l -- l}   ( delete       )each
LSet.union 		{l l -- l}   ( LSet.insert  )each
LSet.intersection	{l l -- l}   ( dup^ member? )filter nip
LSet.symdiff 		{l l -- l}   2dup LSet.union^^ LSet.intersection  LSet.difference

LSet.any? 		{l l -- b}   LSet.intersection nil? not


;==============================================================================
; :section:   Module
;------------------------------------------------------------------------------
module Module
interface _{}

	:p {Module (name docstring) -- Module b}
		Symbol.undotted
		| dup length 2 = not		| zap false
		| dup first over2  = not	| zap false
		| second Symbol.underscore? 	|     false
		others				|     true
api {m --  // (name * docstring) list of module api, without underscored names}
		allfuncs first~   (p)filter   
		nip

	:?module {func -- b}
		first Symbol.undotted
		(( length 2 = )( first Symbol.upper-case? ))and* nip
names {}
		allfuncs ( ?module )filter
		( first Symbol.undotted first )map
		nub sort
		
;--------------------------------------------------
	:?undefine { m func -- }
		first dup Symbol.undotted first over2 = (Root.undefine zap)(zap)if 
undefine {m --  // undefine all words of module m.}
		allfuncs ( ?undefine )each zap
		
end-of-module Module

;==============================================================================
; :section:   Ts
;------------------------------------------------------------------------------
module Ts
interface _{}
import-module {ts api -- ts  // inline-P with api}
		( dup Symbol.undotted second {} swap rotate triple )map flatten 
		swap inline-P
end-of-module Ts

;==============================================================================
; :section:   LInt
;------------------------------------------------------------------------------
module LInt

add {} LInt.add
sub {} LInt.sub
mul {} LInt.mul
divmod {} LInt.divmod
div {} LInt.divmod zap

interface _{}
__info {} "LInt
Operations on unsigned 62-bit long integer.

Some tests in 'tests.rabbit': 'LInt.__test'.

Multiply by shift-left add.
Division by shift-left subtract.

Overflow protection in true fvm-style: error-message and exit.

long maxint = 2^62 - 1

No checking for valid input,
that is all operators must be positive in both halfwords (ih il).
"

;------------------------------------------------------------------------------
<int {i -- (0 il)}    0 swap pair
>int {(ih il) -- i}   unpair   swap 0 = "LInt.>int: overflow"?ok

maxint {-- (ih il)}   Root.maxint Root.maxint pair

;------------------------------------------------------------------------------
	:>digit {i -- c}  48 + String.of-charcode
show {(ih il) -- s}
		nil up
		|  (0 10) divmod  >int consup   dup(0 0)= not | recur
		others|#zap
		down ""( >digit String.++ )fold

read {s -- (ih il)}
		String.to-list
		(0 0)( 48 -  <int  swap (0 10)mul   add )fold

		
out {(ih il) --}   show Root.out

;------------------------------------------------------------------------------
sqr {(ih il) -- (ih il) // sqr(i)}  dup mul

	:2div {} 1 LInt.shr
sqrt {(ih il) -- (ih il) // sqrt, (ih il) > (0 3)}
	dup (0 3) LInt.compare 1 = "LInt.sqrt: only long integer i > 3"?ok
	dup 2div   ( 2dup div  dup^ sub   2div    sub )converge   nip

	
end-of-module LInt

;==============================================================================
; :section:   Random
;------------------------------------------------------------------------------
module Random
interface _{}

__info {} "Random
Random number generation

Random number generation with xorshift-RNG.
period: 2^32-1
seed:   seconds since 1.1.1970 at startup time (Date.now)

usage:
	Random.init		set seed to Date.now
	
	i Random.roll		return 0 <= random-number < i
	Random.boolean		return randomly chosen boolean value
	Random.next		return 32 bit integer random number

	Random.__test		print simple graphics of a few random numbers

sample:
	( 6 Random.roll inc )10 repeat  ->  ( 4 6 2 1 5 3 1 1 6 1 )
	

see:
	wikipedia/xorshift
	George Marsaglia: 'Xorshift RNGs' at http://www.jstatsoft.org/v08/i14/paper

"
__man {} __info out 
;-----------------------------------------------------------------------------

init {--  // init random numbers}
		`Random.seed undefine zap
		`Random.seed ({xx})first Date.now single define
		20 roll (next zap)swap times

seed {-- i  // last random number} 3141592
next {-- i  // return next random number}
		seed
		dup  1 Bit.shl Bit.xor 	; 13     not: 1 19 1
		dup 19 Bit.shr Bit.xor	; 17
		dup  3 Bit.shl Bit.xor	;  5
		`seed undefine zap
		dup single `seed ({last random number})first rolldown define

roll {i -- i  // return 0 <= random number < i}   	next swap   mod abs
boolean {-- b // maybe true, maybe false, who knows...} next        odd?
	
;--------------------------------------------------

let
	x {-- i} Dot.width
	y {-- i} Dot.height
	z {-- i} 26
	>letter {i -- s  // 1 -> A,  26 -> Z} 65 + String.of-charcode
	k {-- i}      y x  * 2/
in
__test-1 {}
		Dot.clr
		( y roll inc  x roll inc  Dot.go  z roll >letter print )k times
		Dot.done

__test-2 {}
	Dot.clr
	(
		( boolean )100 repeat   ()filter length   50 -       y 2/ +
		( boolean )100 repeat   ()filter length   50 -   2*  x 2/ +
		Dot.go
		( boolean )100 repeat   ()filter length   50 -       z 2/ +
		>letter print
	)50 times
	Dot.done

__demo {} __test-1 10 sleep  __test-2
funcs

end-of-module Random

	
;------------------------------------------------------------------------------
;==============================================================================
] Define.defs-x
;==============================================================================
