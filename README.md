# README #

# freelang-mode.el #


Emacs syntax highlighting for Freelang, a programming language for
the **Freeputer Virtual Machine (FVM)**.

File: `emacs/freelang-mode.el`

See [www.Freeputer.net](http://www.freeputer.net/)
or [Freeputer-at-bitbucket](https://bitbucket.org/RobertGollagher/freeputer/overview) .



# Rabbit-vm on Freeputer #

## A Joy programming language ##

Rabbit-vm is essentially an implementation of the concatenative programming language Joy.

Joy is a functional programming language which is not based on the
application of functions to arguments but on the composition of functions.
Function composition is done by concatenation.

See [Joy-at-wikipedia](https://en.wikipedia.org/wiki/Joy_%28programming_language%29)
or [An informal tutorial on Joy](http://www.complang.tuwien.ac.at/anton/euroforth/ef01/thun01.pdf).


The main difference to Joy is Rabbit-vm allows to define new definitions dynamically.
Based on that it is natural to take new definitions as data in quotations and
process that data before you define.

Minor differences are parenteses ()  instead of brackets [] or different meanings of
words ( zap == drop).
Rabbit-vm does not try to be compatible to existing implementations of Joy.

Rabbit-vm on Freeputer arose in order to explore Freeputer as virtual plattform
for Rabbit-vm. Rabbit-vm on Freeputer is by far not ready to use for any productive coding.
But the concept works!

`rabbit/stdlib.rabbit` documents all words build into Rabbit-vm and defines
additional words in rabbit language.



## Files ##

**rabbit-run**
A simple bash script to make life easier with compiler, rom.fp, std.imp etc.


**freelang/rabbit-vm.fl**
The rabbit virtual machine.


**rabbit-vm-fvm.tar.gz**
Tarball containing all files nessassary to compile and run.


## Install ##

Create a directory, unpack **rabbit-vm-fvm.tar.gz** there and say:

`./rabbit-run`.
Without parameters it will print further help.

or `./rabbit-run -mi`.
Rabbit-vm should start and enter repl with a rabbit> prompt.
