;;=============================================================================
;;
;; rabbit-mode.el
;; emacs syntax highlighting for rabbit
;;
;;=============================================================================
;;
;; README
;;
;; Rabbit is a programming language running on rabbit-vm.
;;
;; This file defines rabbit-mode for emacs. It does:
;;
;;  * syntax highlighting.
;;  * load files with file-extension '.rabbit' as rabbit-files.
;;
;;
;; INSTALL
;;
;; Put this file in your emacs load path and the following line
;; into your .emacs file.
;;
;;      (require 'freelang-mode)
;;
;;
;; CONTACT
;;
;; https:/bitbucket.org/sts-q
;;
;;
;; LICENCE
;;
;; GPL 3.0
;;
;;=============================================================================

(add-to-list 'auto-mode-alist '("\\.rabbit\\'" . rabbit-mode))

;;=============================================================================

(add-hook 'rabbit-mode-hook
	  (lambda ()
	    (define-key rabbit-mode-map (kbd "<return>")   'align-newline-and-indent)
	    (define-key rabbit-mode-map (kbd "M-i")        'rabbit-indent-line-fun)
	    (define-key rabbit-mode-map (kbd "<tab>")      (lambda() (interactive) (insert "\t")))
	    ))

;;=============================================================================

(defface font-lock-docstring-face   '((t (:foreground "#32cd32")))   "red")

(defun rabbit-font-lock-fun()
  "return a list"
  (let* ((head   "\\<")     ; \\([[a-zA-Z0-9-_?]+\\s-+]*\\)     
	 (tail   "\\>")
	 (to-end-of-line ".*$")
	 ( faces 
	   `( 
	     
	     (,(concat "^====+" )              . font-lock-keyword-face)
	     (,(concat "^----+" )              . font-lock-keyword-face)
	     (,(concat "^### " to-end-of-line) . font-lock-keyword-face)
	     (,(concat "^## "  to-end-of-line) . font-lock-keyword-face)
	     (,(concat "^# "   to-end-of-line) . font-lock-keyword-face)
	     
;;;    :def {} 
;;;	     (,(concat "\\(:[a-zA-Z0-9-´_?\\+\\<\\>\\*/=\\.%]+\\)\\s-*\\({[^}]*}\\)")         ; {[^}]*}
;;;	      (1  font-lock-type-face)
;;;	      (2  font-lock-doc-face))
	     
	     (,(concat "\\([a-zA-Z0-9-´_?\\+\\<\\>\\*/=\\.%~]+\\)\\s-*\\({[^}]*}\\)")         ; {[^}]*}
	      (1  font-lock-function-name-face)
	      (2  font-lock-doc-face))

;	     (,(concat head "[a-zA-Z0-9-´_?\\.\\+\\<\\>\\*/=]+\\:" tail).  font-lock-function-name-face)
	     (,(concat head "[a-zA-Z0-9-´_?\\.\\+\\<\\>\\*/=]+\\:" tail).  font-lock-variable-name-face)
	     (,(concat  "{[^}]*}" )                                     .  'font-lock-doc-face)
	     (,(concat head (regexp-opt '("BEGIN" "END" "defs" "xdefs" "Letf.define" "Parts.define"
					  "let" "in" "loc" "with" "where" "funcs" "functions"
					  "module" "interface" "implementation" "end-of-module"
					  "loop" "loop^" "loop^^"  "recur" "rec"
					  "testing"
					  "TX") t) tail)		.  font-lock-keyword-face)
	     (,(concat head (regexp-opt '("halt" "look" "xx"
					  "?" "?/" "??" "?/?" "??/" "?/?/" "??//"
					  "?stk" "?2stk" "?3stk" "?4stk"
					  ) t) tail)                      .  font-lock-warning-face)
					;	   (,(concat  "[\\[\\]]" )                                                      .  font-lock-keyword-face)
					;	   (,(concat  "[\\[\\]]" )                                                      .  font-lock-keyword-face)
	     (,(concat head "[\\,][a-zA-Z0-9-´_?\\.\\+\\<\\>\\*/=]+" tail ) . font-lock-comment-face)

	     
	     (,(concat  "[()|]" )                                         .  font-lock-comment-face)
	     (,(concat head (regexp-opt '("->"
					  "define" "begin" "end"
					  "if"     "if*"     "if/" "ifnot" "ifnot*" "ifnot/"
					  "unless" "unless*" "unless/"
					  "when"   "when*"   "when/"
					  "cond"   "cond/"   "cond*"   "case" "else" "others" 
					  "while"  "whilenot"  "until" "untilnot"
					  "linrec" "tailrec" "binrec" "condrec" "unirec" "unirecdo"
					  "go==0" "go!=0"
					  "go/==/"
					  "go"
					  ) t) tail) .  font-lock-builtin-face)
	     (,(concat head "-[0-9\\.]+" tail)                            .  font-lock-builtin-face)
					;	   (,(concat "[#]" )								.  font-lock-constant-face)
	     (,(concat "[#]" )					          .  font-lock-builtin-face)
	     (,(concat head "[0-9\\.]+" tail)                             .  font-lock-constant-face)
	     (,(concat head (regexp-opt '("true" "false"
					  "nothing" "success" "failure"
					  "pad" "mark" ) t) tail)         .  font-lock-constant-face)
	     ))
	 )
    faces))

;;=============================================================================

(defun rabbit-indent-line-fun()
  "Indent current line for rabbit files."
  (interactive)
  (insert "[rabbit-indent-line-fun]")
  nil)

;;=============================================================================

(define-derived-mode rabbit-mode prog-mode "rabbit" 
  "Major mode for editing rabbit files."
  (setf font-lock-defaults       (list (rabbit-font-lock-fun)))
  )


(mapcar (lambda (p)
	  (modify-syntax-entry (car p) (cadr p) rabbit-mode-syntax-table))
	'((?:  "w"  )      ; : is word char --> my:word is a word
	  (?_  "w"  )      ; _ is word char --> my_word is a word
	  (?+  "w"  )
	  (??  "w"  )
	  (?-  "w"  )
	  (?*  "w"  )
	  (?/  "w"  )
	  (?^  "w"  )
	  (?>  "w"  )
	  (?<  "w"  )
	  (?=  "w"  )
	  (?.  "w"  )
	  (?,  "w"  )
	  (?~  "w"  )
	  (?'  "\"" )      ; ' ist string-escape char
	  (?;  "< b")      ; ; starts comment
	   (?\n "> b")      ; ; comment up to end of line
	   ))


(provide 'rabbit-mode)
;;=============================================================================
