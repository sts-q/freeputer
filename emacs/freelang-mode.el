;;=============================================================================
;;
;; freelang-mode.el
;; emacs syntax highlighting for freelang
;;
;;=============================================================================
;;
;; README
;;
;; Freelang is a programming language for the Freeputer Virtual Machine (FVM).
;;
;; This file defines freelang-mode for emacs. It does:
;;
;;  * syntax highlighting.
;;  * load files with file-extension '.fl' as freelang-files.
;;  * make tab-key insert 4 spaces as freelang does not like tabs.
;;
;;
;; INSTALL
;;
;; Put this file in your emacs load path and the following line
;; into your .emacs file.
;;
;;      (require 'freelang-mode)
;;
;;
;; CONTACT
;;
;; https:/bitbucket.org/sts-q
;;
;;
;; LICENCE
;;
;; GPL 3.0
;;
;;=============================================================================

;; load files with file-extension '.fl' as freelang-files.
;; alternative:  put the following into first or second line of your
;;               freelang source file.
;;
;;     -*- mode: freelang -*-
;;

(add-to-list 'auto-mode-alist '("\\.fl\\'" . freelang-mode))

;;=============================================================================

;; let <tab> key insert 4 spaces as freelang does not like tabs.
(add-hook 'freelang-mode-hook
	  (lambda ()
	    (define-key freelang-mode-map (kbd "<tab>")
	      (lambda() (interactive) (insert "    ")))))

;;=============================================================================


(defun freelang-font-lock-fun()
  "return a ( regexp . color ) list"
  (let* ((head   "\\<")                
	 (tail   "\\>")
	 (word   "[[:word:]]+")
	 (stack-comment   "( [^)]* )")
	 (faces 
	  `( 
	    
	    ;; ---------------------------------------------------------
	    
	    ;; jump destination
	    (,(concat "\\:" word)  .  font-lock-builtin-face)
	    
	    ;; label
	    (,(concat word "\\:")  .  font-lock-variable-name-face)
	    
	    ;; function definition
	    (,(concat "\\(\\: " word "\\)\\s-+\\(" stack-comment "\\)")
	     (1  font-lock-function-name-face)    ;;; name
	     (2  font-lock-doc-face))             ;;; stack comment
	    
	    ;; namespace
	    (,(concat word "{\\W")  .  font-lock-keyword-face)
	    (,(concat "\\W}" word)  .  font-lock-keyword-face)
	    
	    ;; ---------------------------------------------------------
	    
	    ;; warning
	    (,(concat head (regexp-opt
			    '("tron" "troff"
			      "xx"
			      ) t) tail)  .  font-lock-warning-face)
	    
	    ;; jump condition
	    (,(concat head (regexp-opt
			    '(
			      "go"
			      "go[>0]" "go[>=0]" "go[==0]" "go[!=0]" "go[<=0]" "go[<0]"
			      "go[>]" "go[>=]" "go[==]" "go[!=]" "go[<=]" "go[<]"
			      "go>0" "go>=0" "go==0" "go!=0" "go<=0" "go<0"
			      "go>" "go>=" "go==" "go!=" "go<=" "go<"

			      ;;;---------------------------------------------
			      ;;; not part of freeputer instruction set
                              ;;; delete or outcomment if not wanted
			      "ifsome"   "ifnil"   "iffail"   "ifnone"   "ifany"   "ifok"
			      "if7some7" "if7nil7" "if7fail7" "if7none7" "if7any7" "if7ok7"
			      "return" "drop_return" "drop2_return" "spop_drop2_return"
			      ;;;---------------------------------------------
			      "slotFloor"
			      ;;;---------------------------------------------
			      ) t) tail)  .  font-lock-builtin-face)
	    
	    ;; fvm instruction set
	    (,(concat head (regexp-opt
			    '(
			      "==="
			      "reador" "writor" "tracor" "getor" "putor"
			      "readorb" "writorb" "tracorb" "getorb" "putorb" 
			      
			      "ret" "invoke" "[invoke]" "fly"
			      
			      "swap" "over" "rot" "tor" "leap" "nip" "tuck"

			      
			      ;; fvm 1.1
			       "rot4" "tor4" "rev4" "swap2" "zoom"
			       "[fly]" "pc?"
			       "math" "trap" "die"
			       "pchan?""gchan?""wchan?""rchan?"
			       "read?" "write?" "get?" "put?"
			       "trace?"

			       			      
			      "rev"
			      "rpush" "rpop"
			      "drop" "drop2" "drop3" "drop4"
			      "dup" "dup2" "dup3" "dup4"
			      "hold" "hold2" "hold3" "hold4"
			      "speek" "speek2" "speek3" "speek4"
			      "spush" "spush2" "spush3" "spush4"
			      "spop" "spop2" "spop3" "spop4"
			      
			      "dec" "decw" "dec2w" "inc" "incw" "inc2w"
			      
			      "@" "!" "[@]" "@b" "!b" "[@b]"
			      "@@" "@!" "[@@]" "@@b" "@!b" "[@@b]"
			      
			      "+" "-" "*" "/" "%" "/%"
			      "[+]" "[-]" "[*]" "[/]" "[%]" "[/%]"
			      "neg" "abs"
			      "&" "|" "^" "[&]" "[|]" "[^]"
			      "<<" ">>" "[<<]" "[>>]"
			      
			      "move" "fill" "find" "match"
			      "moveb" "fillb" "findb" "matchb"
			      
			      "homio"
			      "rchan" "wchan" "gchan" "pchan"
			      
			      "ecode?" "rcode?" "rom?" "ram?" "map?" "stdblk?"
			      "ds?" "ss?" "rs?" "dsn?" "ssn?" "rsn?"
					;"tron" "troff"  ; we have these at warings
			      "reset" "reboot" "halt" "data"
			      ) t) tail)  .  font-lock-type-face)
	    
	    ;; ---------------------------------------------------------
	    ;; numbers
	    (,(concat head (regexp-opt
			    '("some" "nil" "fail"
			      ) t) tail)  .  font-lock-constant-face)
	    (,(concat head "[\\-]?[0-9]+" tail)  .  font-lock-constant-face)
	    ;; ---------------------------------------------------------
	    
	    )))
    faces))

;;=============================================================================

(define-derived-mode   freelang-mode prog-mode "freelang" 
  "Define major mode for editing freelang files."
  (setf font-lock-defaults       (list (freelang-font-lock-fun)))
)

(mapcar (lambda (p)
	  (modify-syntax-entry (car p) (cadr p) freelang-mode-syntax-table))
	'((?:   "W")     ; take :_?...!@ as word chars
	  (?_   "W")
	  (??   "W")
	  (?+   "W")
	  (?-   "W")
	  (?*   "W")
	  (?/   "W")
	  (?<   "W")
	  (?>   "W")
	  (?=   "W")
	  (?.   "W")
	  (?[   "W")
	    (?]   "W")
	  (?#   "W")
	  (?$   "W")
	  (?!   "W")
	  (?@   "W")
	  
	  (?'    "\"")    ; "'" is string delimiter: highlight 'x' like string "x"
	  (?\\   "< b")   ; start of comment. todo: force a space just behind
	  (?\n   "> b")   ; end of comment
	  
	  ))

(provide 'freelang-mode)
;;=============================================================================
